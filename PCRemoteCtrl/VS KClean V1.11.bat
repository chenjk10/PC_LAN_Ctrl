@echo off  

rem echo 删除所有的debug文件夹
rem for /D /r . %%c in (.) do @if exist "%%c\Debug" rd /S /Q "%%c\Debug"
rem echo 删除所有的Release文件夹
rem for /D /r . %%c in (.) do @if exist "%%c\Release" rd /S /Q "%%c\Release"

rem echo 删除文件项目文件下的临时文件目录Debug
rem for /f "delims=" %%c in ('dir /s /b /ad') do @if exist "%%c\Debug" rd /S /Q "%%c\Debug" && echo "del directory %%c\Debug"
rem echo 删除文件项目文件下的临时文件目录Release
rem for /f "delims=" %%c in ('dir /s /b /ad') do @if exist "%%c\Release" rd /S /Q "%%c\Release" && echo del directory "%%c\Release"

echo 删除ipch
if exist ipch rd /S /Q ipch
rem for /f "delims=" %%a in ('dir /s /b /ad "ipch"') do (rd /q /s "%%a" && echo 删除文件 "%%a")

rem 删除obj
if exist obj rd /S /Q obj

rem 删除tmp dir
if exist bin\Tmp rd /S /Q bin\Tmp

rem 删除MDK-ARM\RTE
if exist MDK-ARM\RTE rd /S /Q MDK-ARM\RTE
if exist MDK-ARM\DebugConfig rd /S /Q MDK-ARM\DebugConfig

rem 删除bin
rem if exist bin rd /S /Q bin
if exist bin\uires rd /S /Q bin\uires
del /F /S /Q /A bin\*.exp
del /F /S /Q /A bin\*.pdb
del /F /S /Q /A bin\*.exe
del /F /S /Q /A bin\*.dll
del /F /S /Q /A bin\*.lib
del /F /S /Q /A bin\*.lik


set SPECIAL_SUFFIX=*.aps *.idb *.ncp *.obj *.pch *.sbr *.pdb *.bsc *.ilk *.res *.ncb *.opt *.suo *.manifest *.dep *.clw *.plg *.positions *.ldb *.exp  *.ipch BuildLog.htm *.sdf
set SPECIAL_SUFFIX2=*.lastbuildstate *.lock *.map *.meta *.VC.db *.write.1u.tlog
set SPECIAL_SUFFIX3=*.log *.tmp *.1.tlog *.unsuccessfulbuild
set SPECIAL_USER_DATA=*.vcproj.*.*.user ResolveAssemblyReference.cache
set UPGRADE_FILES=UpgradeLog.XML UpgradeReport.css UpgradeReport.xslt UpgradeReport_Minus.gif UpgradeReport_Plus.gif
set MDK_FILES=*.uvguix.* *.c.orig *.h.orig EventRecorderStub.scvd startup_*rx.lst *.crf *.d *.o *.iex *.axf *.build_log.htm *.hex *.htm *.lnp *.map *.sct *.dep RTE_Components.h.orig *RETx.dbgconf
set QT_FILES=*.read.1u.tlog
echo 删除特定后缀的文件 %SPECIAL_SUFFIX% %SPECIAL_SUFFIX2% %SPECIAL_SUFFIX3% %UPGRADE_FILES% %SPECIAL_USER_DATA% %MDK_FILES% %QT_FILES%
del /F /S /Q /A %SPECIAL_SUFFIX% %SPECIAL_SUFFIX2% %SPECIAL_SUFFIX3% %UPGRADE_FILES% %SPECIAL_USER_DATA% %MDK_FILES% %QT_FILES%
rem for /r . %%c in (%SPECIAL_SUFFIX% %SPECIAL_SUFFIX2% %SPECIAL_SUFFIX3%) do if exist %%c echo "%%c"



echo 删除所有空目录
for /f "delims=" %%a in ('dir /s /b /ad') do (rd /q "%%a" && echo 删除空文件夹 "%%a")




rem pause
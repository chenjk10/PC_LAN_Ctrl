﻿#pragma once

//#include <thread>
#include <string>
#include "uHttpWebSocket.h"

#include <QtCore/QtCore>
#include "QtChCharset.h"

class QtuHttpWebSocket : public QObject, public uWS::uHttpWebSocket
{
	Q_OBJECT
private:
	QtuHttpWebSocket(QtuHttpWebSocket&);
	QtuHttpWebSocket operator=(QtuHttpWebSocket&);
public:
	QtuHttpWebSocket();
	virtual ~QtuHttpWebSocket();
	// 单例
	static QtuHttpWebSocket& Instance();

protected:
	virtual void onMessage(uWS::WebSocket<uWS::SERVER> *ws, char *message, size_t length, uWS::OpCode opCode) override;
public:
	Q_SIGNALS :
	// 消息
	void SignalWebMsg(QString strType);
};
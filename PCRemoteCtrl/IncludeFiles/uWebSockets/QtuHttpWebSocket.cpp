﻿//#include "stdafx.h"
#include "QtuHttpWebSocket.h"


QtuHttpWebSocket::QtuHttpWebSocket()
{

}

QtuHttpWebSocket::~QtuHttpWebSocket()
{
}

QtuHttpWebSocket & QtuHttpWebSocket::Instance()
{
	static QtuHttpWebSocket instance;
	return instance;
}

void QtuHttpWebSocket::onMessage(uWS::WebSocket<uWS::SERVER> *ws, char *message, size_t length, uWS::OpCode opCode)
{
	std::string strMessage;
	if (message && length > 0)
	{
		strMessage.assign(message, length);
		//ws->send(message, length, opCode);
		emit SignalWebMsg(QString::fromStdString(strMessage));
	}
	
}


#pragma once

#include <QtCore/QProcess>

class CommandProcess : public QProcess
{
	//Q_OBJECT
public:
    explicit CommandProcess(QObject *parent = Q_NULLPTR);
    virtual ~CommandProcess();
public:
    // 执行命令
    void ExecuteCommand(const QString &command);
    bool ExecuteCommandWaitFinish(const QString &command, int waitMs = 30000);
    bool ExecuteCommandWaitResult(const QString &command, QByteArray& result, int waitMs = 30000);
public:
    // 静态函数
    // 执行命令
    static int StaticExecuteCommand(const QString &command);
    static int StaticExecuteProgram(const QString &program, const QStringList &arguments);

    // 执行命令并等待结果
    static bool StaticExecuteCommandWaitResult(const QString &command, QByteArray& stdOutResult, QByteArray& stdErrResult, int& exitCode, int waitMs = 30000);
    static bool StaticExecuteCommandWaitResult(const QString &command, QByteArray& result, int& exitCode, int waitMs = 30000);

    static bool StaticExecuteProgramWaitResult(const QString &program, const QStringList &arguments, QByteArray& stdOutResult, QByteArray& stdErrResult, int& exitCode, int waitMs = 30000);
    static bool StaticExecuteProgramWaitResult(const QString &program, const QStringList &arguments, QByteArray& result, int& exitCode, int waitMs = 30000);
public:
    // 运行程序
    void ExecuteProgram(const QString &program, const QStringList &arguments);
    // 给运行中的程序发送命令
    bool WriteCommand(const char *data);
    bool WriteCommand(QString strCommand);

};




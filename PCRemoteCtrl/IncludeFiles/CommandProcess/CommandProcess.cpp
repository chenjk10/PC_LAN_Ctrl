#include "CommandProcess.h"

#include <QtCore/QDebug>


CommandProcess::CommandProcess(QObject *parent)
    : QProcess(parent)
{

}

CommandProcess::~CommandProcess()
{

}

void CommandProcess::ExecuteCommand(const QString &command)
{
    start(command);
}

bool CommandProcess::ExecuteCommandWaitFinish(const QString &command, int waitMs)
{
    start(command);
    if(!waitForStarted(waitMs))
    {
        qCritical() << "ExecuteCommandWaitFinish waitForStarted timeout";
        return false;
    }
    if(!waitForFinished(waitMs))
    {
        qCritical() << "ExecuteCommandWaitFinish waitForFinished timeout";
        return false;
    }
    return true;
}

bool CommandProcess::ExecuteCommandWaitResult(const QString &command, QByteArray& result, int waitMs)
{
    start(command);
    if(!waitForStarted(waitMs))
    {
        qCritical() << "ExecuteCommandWaitResult waitForStarted timeout";
        return false;
    }
    if(!waitForFinished(waitMs))
    {
        qCritical() << "ExecuteCommandWaitResult waitForFinished timeout";
        return false;
    }

    result = readAll();

    return true;
}

int CommandProcess::StaticExecuteCommand(const QString &command)
{
    return QProcess::execute(command);
}

int CommandProcess::StaticExecuteProgram(const QString &program, const QStringList &arguments)
{
    return QProcess::execute(program, arguments);
}

bool CommandProcess::StaticExecuteCommandWaitResult(const QString &command, QByteArray &stdOutResult, QByteArray &stdErrResult, int &exitCode, int waitMs)
{
    QProcess process;
    process.start(command);
    if(!process.waitForStarted(waitMs))
    {
        qCritical() << "StaticExecuteCommandWaitResult waitForStarted timeout. " << command;
        return false;
    }
    if(!process.waitForFinished(waitMs))
    {
        qCritical() << "StaticExecuteCommandWaitResult waitForFinished timeout. " << command;
        return false;
    }
    if(&stdOutResult == &stdErrResult)
    {// 标准输出和错误输出是同一个对象，需要清空原数据，然后以添加的方式赋值
        stdOutResult.clear();
        stdErrResult.clear();

        stdOutResult += process.readAllStandardOutput();
        stdErrResult += process.readAllStandardError();
    }
    else
    {
        stdOutResult = process.readAllStandardOutput();
        stdErrResult = process.readAllStandardError();
    }
    //result = process.readAll();
    exitCode = process.exitCode();
    //qDebug() << command << " exitCode:" << process.exitCode();
    //qDebug() << command << " exitStatus:" << process.exitStatus();
    if(process.exitStatus() != QProcess::NormalExit)
    {
        qCritical() << "StaticExecuteProgramWaitResult isn't NormalExit. " << command;
        return false;
    }
    return true;
}

bool CommandProcess::StaticExecuteCommandWaitResult(const QString &command, QByteArray &result, int& exitCode, int waitMs)
{
    return StaticExecuteCommandWaitResult(command, result, result, exitCode, waitMs);
}

bool CommandProcess::StaticExecuteProgramWaitResult(const QString &program, const QStringList &arguments, QByteArray &stdOutResult, QByteArray &stdErrResult, int &exitCode, int waitMs)
{
    QProcess process;
    process.start(program, arguments);
    if(!process.waitForStarted(waitMs))
    {
        qCritical() << "StaticExecuteProgramWaitResult waitForStarted timeout. " << program << " " << arguments;
        return false;
    }
    if(!process.waitForFinished(waitMs))
    {
        qCritical() << "StaticExecuteProgramWaitResult waitForFinished timeout. " << program << " " << arguments;
        return false;
    }

    if(&stdOutResult == &stdErrResult)
    {// 标准输出和错误输出是同一个对象，需要清空原数据，然后以添加的方式赋值
        stdOutResult.clear();
        stdErrResult.clear();

        stdOutResult += process.readAllStandardOutput();
        stdErrResult += process.readAllStandardError();
    }
    else
    {
        stdOutResult = process.readAllStandardOutput();
        stdErrResult = process.readAllStandardError();
    }
    //result = process.readAll();
    exitCode = process.exitCode();
    //qDebug() << command << " exitCode:" << process.exitCode();
    //qDebug() << command << " exitStatus:" << process.exitStatus();
    if(process.exitStatus() != QProcess::NormalExit)
    {
        qCritical() << "StaticExecuteProgramWaitResult isn't NormalExit. " << program << " " << arguments;
        return false;
    }
    return true;
}

bool CommandProcess::StaticExecuteProgramWaitResult(const QString &program, const QStringList &arguments, QByteArray& result, int& exitCode, int waitMs)
{
    return StaticExecuteProgramWaitResult(program, arguments, result, result, exitCode, waitMs);
}

void CommandProcess::ExecuteProgram(const QString &program, const QStringList &arguments)
{
    start(program, arguments);
}

bool CommandProcess::WriteCommand(const char *data)
{
    qint64 len = strlen(data);
    qint64 iWriteLen = write(data);
    if(len != iWriteLen)
    {
        qCritical() << "CommandProcess::WriteCommand failed.len != iWriteLen." << len << "!=" << iWriteLen;
        qCritical() << "CommandProcess::WriteCommand" << data;
    }
    //qInfo() << "WriteCommand:" << data;
    return (len == iWriteLen);
}

bool CommandProcess::WriteCommand(QString strCommand)
{
    //QByteArray command = strCommand.toLocal8Bit();
    //QByteArray command = strCommand.toLatin1();
    QByteArray command = strCommand.toUtf8();

    qint64 len = command.size();
    qint64 iWriteLen = write(command);
    if(len != iWriteLen)
    {
        qCritical() << "CommandProcess::WriteCommand2 failed.len != iWriteLen." << len << "!=" << iWriteLen;
        qCritical() << "CommandProcess::WriteCommand2:" << strCommand;
    }
    //qInfo() << "WriteCommand2:[" << len << "]:" << command;
    return (len == iWriteLen);
}










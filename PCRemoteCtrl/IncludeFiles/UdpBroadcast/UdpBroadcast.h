﻿#ifndef UDPBROADCAST_H
#define UDPBROADCAST_H

#include <QObject>
#include <QVector>
#include <QSharedPointer>
#include <QtNetwork/QUdpSocket>
#include <QtNetwork/QHostInfo>

class UdpBroadcast : public QObject
{
    Q_OBJECT
public:
    static UdpBroadcast * Singleton();
public:
    UdpBroadcast(QObject *parent = nullptr);
    ~UdpBroadcast();
signals:
    void signalNewServer(QString serverName, QString serverAddr);

public slots:
    bool InitClient(QVector<quint16> serverPortVec);
    bool InitServer(QString serverName, QString serverAddr, QVector<quint16> serverPortVec);

    void scan();
private slots:
    void slotClientReadyRead();
    void slotServerReadyRead();
private:
    QUdpSocket * pudpClientSocket = nullptr;
    QVector<quint16> clientServerPortVec;   // 广播用端口
    QVector<QSharedPointer<QUdpSocket>> pudpServerSocketVec;
    QVector<quint16> serverPortVec;         // 监听端口
    QString serverName;
    QString serverAddr;
};

#endif // UDPBROADCAST_H

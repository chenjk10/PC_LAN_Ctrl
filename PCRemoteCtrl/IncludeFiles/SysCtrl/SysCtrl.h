﻿#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <QtCore/QObject>
#include "QtChCharset.h"



class SysCtrl : public QObject
{
	Q_OBJECT
private:
	SysCtrl(SysCtrl&);
	SysCtrl operator=(SysCtrl&);
public:
	SysCtrl();
	virtual ~SysCtrl();
public:
	// 获取单例
	static SysCtrl& Instance();
private:
	// ExitWindowsEx
	static bool KExitWindowsEx(unsigned int uFlags);
	// 获取关机权限
	static bool GetShutdownPrivilege();
	// cmd命令参数
	static bool SysCmd(QString cmd);
public:
	// 关机
	static bool SysShutdown(bool bForce); 
	static bool SysShutdownCmd(bool bForce);
	// 重启
	static bool SysReboot(bool bForce);
	static bool SysRebootCmd(bool bForce);
	// 注销
	static bool SysLogoff(bool bForce);
	static bool SysLogoffCmd(bool bForce);
	// 锁定电脑
	static bool SysLock();
	// 休眠/挂起
	//bHibernate: If this parameter is TRUE, the system hibernates.If the parameter is FALSE, the system is suspended.
	static bool SysSuspend(bool bHibernate, bool bForce);
	///////////////////////////////////////////////////////////////
public:
	// keybd_event()
	static bool KeyBoardEvent(std::vector<int> keyVec);
	// SendInput
	static bool KeyBoardSendInput(std::vector<int> keyVec);
	// PostMessage，组合键有问题，暂时不知原因
	static bool KeyBoardPostMessage(std::vector<int> keyVec);

	// 发送按键到系统前端窗口
	bool KeyBoardForegroundWindow(QString strKeys);

	// 组合或者单一按键转换成按键值
	std::vector<int> ParseKeys(QString strKeys);
	// 初始化mKeyValMap
	void InitKeyValMap();
	// 获取按键对应按键值或者按键组合，strKey为单一按键
	bool GetKeyVal(QString strKey, std::vector<int>& keysVec);
	bool GetKeyVal(std::string strKey, std::vector<int>& keysVec);

private:
	// <按键文本,按键值Vec>
	std::unordered_map<std::string, std::vector<int>> mKeyValMap;
};
﻿#pragma once


#include <string>
#include <vector>

class QrEncode
{
public:
	enum QrEncodeType{
		QrEncodeType_BMP,
		QrEncodeType_PNG,
		QrEncodeType_SVG,
		QrEncodeType_TIFF,
		QrEncodeType_DIGIT,	// 数字01
		QrEncodeType_PBM,	// 数字01、空格
		QrEncodeType_ASCII, // ASCII，X和空格
		QrEncodeType_JSON,	// JSON结构
	};
private:
	QrEncode();
	~QrEncode();
public:
	static QrEncode& Instance();
private:
	QrEncode& operator=(const QrEncode&);
private:
	typedef struct qrcode_t QRCode;
	QRCode * InitQr(const std::string& strDataUtf8);
public:
	// iSeparator: 像素之间的距离, separator pattan width(0 - 16, default: 4)
	// iMagnifying: 放大比例，magnifying ratio (1-16, default: 1)

	// 返回指定类型结果
	std::vector<unsigned char> ToQrEncode(const std::string& strDataUtf8, QrEncodeType iEncodeType, int iSeparator = 4, int iMagnifying = 1);
public:
	const std::string GetErr();
	void SetErr(std::string strErr);
private:
	std::string mstrErr;
};


﻿#pragma once

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSystemTrayIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QMenu>

#include "ui_PCRemoteCtrl.h"

#include <jsoncpp-1.9.1/json/json.h>
#include "UdpBroadcast/UdpBroadcast.h"

enum StatusBarMsgColor {
	StatusBarMsgColor_Black,
	StatusBarMsgColor_Green,
	StatusBarMsgColor_Blue,
	StatusBarMsgColor_Red,
};


class PCRemoteCtrl : public QMainWindow
{
	Q_OBJECT

public:
	PCRemoteCtrl(QWidget *parent = Q_NULLPTR);

private Q_SLOTS:
	// 状态栏显示消息
	void SlotStatusBarMessage(QString strMessage, StatusBarMsgColor iLevel = StatusBarMsgColor_Black, int iTimeoutMs = 8000);
private Q_SLOTS:
	// 网页消息
	void SlotWebMsg(QString);
public:
	// 创建二维码
	void CreateQrCode();
public:
	// 读取json配置文件
	void ReadJsonConfig();
	int miWebPort = 80;
	std::string mstrKeyBoards;
private:
	// web根目录
	QString mstrWebRoot;
	// web socket 初始化
	void InitWebsocket();
private:
	// udp 广播
	UdpBroadcast udpBroadcast;
	void InitUdpBroadcast();
private:
	// 托盘
	QAction *minimizeAction;
	//QAction *maximizeAction;
	QAction *restoreAction;
	QAction *quitAction;
	QMenu *trayIconMenu = nullptr;
	QSystemTrayIcon * trayIcon = nullptr;
	void InitTray();
protected:
	virtual void changeEvent(QEvent * event);
public:
	// JSON获取值
	static QString GetJsonStrVal(Json::Value& node, const std::string strNodeName);
	std::string GetJsonStdStrVal(Json::Value& node, const std::string strNodeName);
	static int GetJsonIntVal(Json::Value& node, const std::string strNodeName);
	static __int64 GetJsonInt64Val(Json::Value& node, const std::string strNodeName);
	static double GetJsonDoubleVal(Json::Value& node, const std::string strNodeName);
	static bool GetJsonboolVal(Json::Value& node, const std::string strNodeName);
	static bool IsJsonMemberValid(Json::Value& node, const std::string strNodeName);
protected:
	// Event handlers
	//virtual void mousePressEvent(QMouseEvent *event);
	//virtual void keyPressEvent(QKeyEvent *event);
	// 关闭事件
	virtual void closeEvent(QCloseEvent *event);
private:
	Ui::PCRemoteCtrlClass ui;
};



class WebsocketProcess
{
private:
	WebsocketProcess(WebsocketProcess&);
	WebsocketProcess operator=(WebsocketProcess&);
public:
	WebsocketProcess();
	virtual ~WebsocketProcess();
public:
	static QSharedPointer<WebsocketProcess> Instance();
public:
	void SendToAllClient(std::string strData);
public:
	void SendStatusBarMsg(QString strMessage, StatusBarMsgColor iLevel, int iTimeoutMs);
public:
	// 关闭弹窗
	void SendCloseModal();
};
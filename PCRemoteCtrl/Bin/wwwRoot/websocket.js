﻿

// websocket连接对象
let kWebSocket = null;//KNewWebSocket();
//KSendMsg('hello');

// 弹窗ID
let gModalDialogID = null;



///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////


// 显示错误信息
function ShowErrInfo(err, iColorType, iTimeoutMs) {
    // iColorType:
    // 0: Black,
    // 1: Green,
    // 2: Blue,
    // 3: Red,
    if (err.length != 0) {
        if ("undefined" == typeof iColorType) {
            iColorType = 3;
            //console.log('iColorType is undefined');
        }
        if ("undefined" == typeof iTimeoutMs) {
            iTimeoutMs = 8000;
        }
        let strOut = "";
        strOut += "<div class=\"alert alert-warning alert-dismissible fade show mb-0\" id=\"ErrMsgInner\" role=\"alert\">";
        switch (iColorType) {
            case 0:
                strOut += " <span class=\"font-weight-bold text-dark\">";
                break;
            case 1:
                strOut += " <span class=\"font-weight-bold text-success\">";
                break;
            case 2:
                strOut += " <span class=\"font-weight-bold text-info\">";
                break;
            case 3:
                strOut += " <span class=\"font-weight-bold text-danger\">";
                break;
            default:
                strOut += " <span class=\"font-weight-bold text-danger\">";
                break;
        }
        strOut += err;
        strOut += " </span>";
        strOut += " <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">";
        strOut += "     <span aria-hidden=\"true\">&times;</span>";
        strOut += " </a>";
        strOut += "</div>";
        document.getElementById("ErrMsg").innerHTML = strOut;

        let iScrollTop = $(document).scrollTop();  //获取滚动条位置
        //if (0 == iScrollTop)
        {
            //console.log($("#ErrMsg").outerHeight(true));
            let spaceLeight = $("#ErrMsg").outerHeight(true);
            //spaceLeight -= parseInt($("#ErrMsg div").css('margin-bottom'));
            $("#ErrMsgSpace").height(spaceLeight);
            $('#ErrMsgSpace').show();
        }
        // 监控关闭按钮
        $('#ErrMsgInner').on('closed.bs.alert', function () {
            $('#ErrMsgSpace').hide();
        });
    } else {
        document.getElementById("ErrMsg").innerHTML = '';
        $('#ErrMsgSpace').hide();
    }
};


// 新建websocket连接
function KNewWebSocket() {
    //let websocketUrl = 'ws://localhost:7681';
    //var websocketUrl = 'ws://127.0.0.1:7681';
    //return new WebSocket(websocketUrl);
    let websocketUrl = 'ws://' + window.document.location.host;//document.getElementById('serverAddr').value;
    let protocol = 'lws-minimal';//document.getElementById("websocketProtocols").value;
    if (protocol.length > 0) {
        return new WebSocket(websocketUrl, protocol);//, 'lws-minimal');
    }
    else {
        return new WebSocket(websocketUrl);//, 'lws-minimal');
    }
};

//websocket发送数据以及数据处理
function KSendMsg(msg) {
    if ("WebSocket" in window) {
        //alert("您的浏览器支持 WebSocket!");
        if (null == kWebSocket) {
            // 打开一个 web socket
            //var ws = new WebSocket("ws://localhost:9998/echo");
            //var ws = KNewWebSocket();//new WebSocket('ws://192.168.0.129:7681', 'lws-minimal');
            kWebSocket = KNewWebSocket();
            kWebSocket.onopen = function () {
                // Web Socket 已连接上，使用 send() 方法发送数据
                kWebSocket.send(msg);
                //console.log("数据发送中...");
                ShowErrInfo('');    // 隐藏错误
            };
            kWebSocket.onerror = function (e) {
                console.log("error " + e);
            };

            kWebSocket.onmessage = WebSocketReceive;

            kWebSocket.onclose = function (e) {
                // 关闭 websocket
                //alert("连接已关闭...");
                console.log("connect closed:" + e.code);
                kWebSocket = null;

                ShowErrInfo('连接已断开');
                if (gModalDialogID != null) {// 关闭弹窗
                    $('#' + gModalDialogID).modal('hide');
                }
                // 禁止所有按钮
                $("#KToolBar button").attr('disabled', true);
            };
        }
        else {
            if (kWebSocket.readyState == 1) {
                kWebSocket.send(msg);
            }
        }
    }
    else {
        // 浏览器不支持 WebSocket
        alert("您的浏览器不支持 WebSocket!");
    }
}

function WebSocketReceive(evt) {
    var received_msg = evt.data;
    //console.log(received_msg);
    //console.log("数据已接收...");
    var objJson = JSON.parse(received_msg); //由JSON字符串转换为JSON对象
    //console.log("收到消息: " + objJson.msgType);

    if (objJson.msgType == 'hello') {// 重新连接或者新连接
        $("#btnShutdown").attr('disabled', false);
        $("#btnSysLogoff").attr('disabled', false);
        $("#btnTestKey").attr('disabled', false);
    }
    else if (objJson.msgType == 'Status') {
        /*
        let bIsOpenedSerial = objJson.SerialOpenStatus;
        let bIsOnTest = objJson.IsOnTest;
        let bOpenFile = objJson.FileIsOpen;

        $("#btnNewFile").attr('disabled', (bIsOnTest) ? true : false);
        $("#btnSysLogoff").attr('disabled', (bIsOnTest) ? true : false);
        $("#btnSearchDevice").attr('disabled', (bIsOnTest) ? true : false);
        !bIsOnTest ? $("#btnStartNewTest").show() : $("#btnStartNewTest").hide();
        !bIsOnTest ? $("#btnStopTest").hide() : $("#btnStopTest").show();
        if (0 == gLanguageIndex) {
            $("#btnStartLabel").text(!bIsOnTest ? "开始测试" : "停止测试");
        } else {
            $("#btnStartLabel").text(!bIsOnTest ? "StartTest" : "StopTest");
        }
        */

    }
    else if (objJson.msgType == "modal-btnClose") {// 关闭弹窗
        if (gModalDialogID != null) {
            $('#' + gModalDialogID).modal('hide');
        }
    }
    else if (objJson.msgType == "ShowCompany") {
        $(".footer").show();
    }
    else if (objJson.msgType == "toolbar-btnShutdown-question") {
        $('#modalShutdownQuestion').modal('show');
    }
    else if (objJson.msgType == "toolbar-btnSysLogoff-question") {
        $('#modalLogoffQuestion').modal('show');
    }
    else if (objJson.msgType == "htmlKeysButton") {
        //$('#collapseKeyBoardButtons .card-body').html(objJson.html);
    }
    else if (objJson.msgType == "sreenShotImg") {
        let bFirstImg = false;
        if ($('#screenShotImg').length <= 0) {
            bFirstImg = true;
        }

        $('#screenShotImgContainer').attr('imgOrgWidth', objJson.imgWidth);
        $('#screenShotImgContainer').attr('imgOrgHeight', objJson.imgHeight);
        
        
        // 多点触摸事件
        function TouchEvent(e) {
            if (2 == e.touches.length) {
                e.preventDefault();
            }

            if ('touchstart' == e.type) {
                if (2 == e.touches.length) {
                    isTouch = true;
                    lastTouches = e.touches;
                }
                else {
                    isTouch = false;
                }
                //lastTouches = e.touches;
                if (e.touches.length == 1) {
                    //    $(".footerText").text('' + e.type + ':' + e.touches.length + ',(' + parseInt(lastTouches[0].clientX) + ',' + parseInt(lastTouches[0].clientY) + ')');
                }
                //$(".footerText").text('' + e.type + ':' + lastTouches.length);
            } else if ('touchmove' == e.type) {

                if (e.touches.length == 1) {
                    //$(".footerText").text('' + e.type + ':' + e.touches.length + ',(' + parseInt(lastTouches[0].clientX) + ',' + parseInt(lastTouches[0].clientY) + ')');
                }
                //$(".footerText").text('' + e.type + ':' + lastTouches.length);
                //$(".footerText").text($(".footerText").text() + ':' + isTouch);

                if (2 == e.touches.length && isTouch) {

                    let lastDistance = Math.sqrt(Math.pow(lastTouches[0].clientX - lastTouches[1].clientX, 2) + Math.pow(lastTouches[0].clientY - lastTouches[1].clientY, 2));
                    let distance = Math.sqrt(Math.pow(e.touches[0].clientX - e.touches[1].clientX, 2) + Math.pow(e.touches[0].clientY - e.touches[1].clientY, 2));

                    let dif = distance - lastDistance;
                    //$(".footerText").text(e.type + '22:' + dif);
                    dif = Math.floor(dif);

                    let imgOrgWidth = parseInt($('#screenShotImgContainer').attr('imgOrgWidth'));
                    let imgOrgHeight = parseInt($('#screenShotImgContainer').attr('imgOrgHeight'));
                    let imgWidth = parseInt($('#screenShotImgContainer').attr('imgWidth'));
                    let imgHeight = parseInt($('#screenShotImgContainer').attr('imgHeight'));

                    imgWidth += dif;
                    imgHeight = parseInt(imgWidth / imgOrgWidth * imgOrgHeight);

                    $('#screenShotImg').css('width', Math.ceil(imgWidth) + 'px');
                    $('#screenShotImg').css('height', Math.ceil(imgHeight) + 'px');

                    // left滚动条
                    let leftIndex = lastTouches[0].clientX < lastTouches[1].clientX ? 0 : 1;
                    let scrollLeftCrt = $('#screenShotImgContainer').scrollLeft();
                    let scrollLeftNew = Math.ceil(scrollLeftCrt + e.touches[leftIndex].clientX - lastTouches[leftIndex].clientX);
                    $('#screenShotImgContainer').scrollLeft(scrollLeftNew);
                    // top滚动条
                    let topIndex = lastTouches[0].clientY < lastTouches[1].clientY ? 1 : 0;
                    let scrollTopCrt = $('#screenShotImgContainer').scrollTop();
                    let scrollTopNew = Math.ceil(scrollTopCrt + e.touches[topIndex].clientY - lastTouches[topIndex].clientY);
                    $('#screenShotImgContainer').scrollTop(scrollTopNew);


                    $(".footerText").text(e.type + ':scrollLeftCrt:' + scrollLeftCrt + ', scrollLeftNew:' + scrollLeftNew);



                    $('#screenShotImgContainer').attr('imgWidth', imgWidth);
                    $('#screenShotImgContainer').attr('imgHeight', imgHeight);

                    /*$(".footerText").text(e.type + ':dif:' + dif + 'width:' + imgWidth + 'height' + imgHeight
                        + ',ls:' + $('#screenShotImgContainer').scrollLeft()
                        );
                        */
                    lastTouches = e.touches;
                }
                else {
                    isTouch = false;
                }
            } if ('touchend' == e.type) {
                isTouch = false;
            }
            if (0) {
                $(".footerText").text('' + e.type + ':' + e.touches.length);
                if (e.touches.length == 1) {
                    $(".footerText").text('' + e.type + ':' + e.touches.length + ',(' + parseInt(e.touches[0].clientX) + ',' + parseInt(e.touches[0].clientY) + ')');
                }
                if (e.touches.length == 2) {
                    $(".footerText").text('' + e.type + ':' + e.touches.length + ',(' + parseInt(e.touches[0].clientX) + ',' + parseInt(e.touches[0].clientY) + ')' + ',(' + parseInt(e.touches[1].clientX) + ',' + parseInt(e.touches[1].clientY) + ')');
                }

                $(".footerText").text($(".footerText").text() + ':' + isTouch);
            }
        }



        if (bFirstImg) {
            $('#screenShotImgContainer').attr('imgWidth', objJson.imgWidth);
            $('#screenShotImgContainer').attr('imgHeight', objJson.imgHeight);

            //$('#screenShotImgContainer').html('<img style=\"width:100%;height:100%;\" src=\"data:image/bmp;base64,' + objJson.imgBase64 + '\"/></p></body></html>');
            $('#screenShotImgContainer').html('<img id="screenShotImg" src=\"data:image/bmp;base64,' + objJson.imgBase64 + '\"/>');
            // 设置容器最大高度
            let imgContainerWidth = $('#collapseScreenShot .card-body').width() - 25;
            let imgContainerHeight = imgContainerWidth / objJson.imgWidth * objJson.imgHeight;
            let maxHeight = $('#screenShotImgContainer').css('max-height');
            maxHeight = parseInt(maxHeight);
            if (maxHeight < imgContainerHeight) {
                maxHeight = imgContainerHeight + 25;
                if (maxHeight < 500) {
                    maxHeight = 500;
                }
                $('#screenShotImgContainer').css('max-height', maxHeight + 'px');
            }
            // 事件监听
            $('#screenShotImg').on("touchstart", TouchEvent);
            $('#screenShotImg').on("touchmove", TouchEvent);
            $('#screenShotImg').on("touchend", TouchEvent);
            // 自适应图片
            ScreenShotAdaption(null);
        }
        else {
            $('#screenShotImg').attr('src', 'data:image/bmp;base64,' + objJson.imgBase64);
        }

        /*
        document.getElementById('screenShotImg').addEventListener("touchstart", function (e) {
            if (e.touches.length >= 2) {  //判断是否有两个点在屏幕上
                //istouch=true;
                //start=e.touches;  //得到第一组两个点

                $(".footerText").text('ccc');

            };
            $(".footerText").text('ddd:' + e.touches.length);
        }, false);
        */

        /*
        var box = document.querySelector("#screenShotImg");
        var boxGesture=setGesture(box);  //得到一个对象
        boxGesture.gesturestart=function(){  //双指开始
            box.style.backgroundColor="yellow";
        };
        boxGesture.gesturemove=function(e){  //双指移动
            box.innerHTML = e.scale+"<br />"+e.rotation;
            box.style.transform="scale("+e.scale+") rotate("+e.rotation+"deg)";//改变目标元素的大小和角度
        };
        boxGesture.gestureend=function(){  //双指结束
            box.innerHTML="";
            box.style.cssText="background-color:red";
        };
        */


    }

        //////////////////////////////////////////
    else if (objJson.msgType == 'ErrInfo') {
        console.log(objJson.ErrInfo);
        //alert(objJson.ErrInfo);
        ShowErrInfo(objJson.ErrInfo);
    }
    else if (objJson.msgType == 'statusBarMsg') {
        console.log(objJson.msg);
        //alert(objJson.ErrInfo);

        ShowErrInfo(objJson.msg, objJson.color, objJson.timeoutMs);
    }

    ////////////////////////////////////////////////////////////////////////
    if (objJson.msgType != 'ErrInfo'
        && objJson.msgType != 'statusBarMsg'
        && objJson.msgType != "modal-btnClose") {
        ShowErrInfo('');    // 隐藏错误
    }
};


function KWindowResize() {
    //console.log($('header').width());

    let iColCount = 6;
    let minBtnSize = 52;

    let toolBarWidth = $('main').width();
    let toolCount = $("#KToolBar figure").length;
    let iNewBtnSize = minBtnSize;

    let iMaxCountPerRow = Math.floor(toolBarWidth / minBtnSize);
    if (iMaxCountPerRow > toolCount) {
        iNewBtnSize = minBtnSize;
    }
    else {
        iNewBtnSize = Math.floor(toolBarWidth / iMaxCountPerRow);
    }

    // 限定最小值
    if (iNewBtnSize < minBtnSize) {
        iNewBtnSize = minBtnSize;
    }

    //iNewBtnSize -= 10;
    // 遍历工具栏的所有按钮
    $(".KToolbarBtn").each(function () {
        $(this).css("width", iNewBtnSize + "px");
        $(this).css("height", iNewBtnSize + "px");
        $(this).css("border-radius", iNewBtnSize / 2 + "px");
    });
    let iNewImgSize = 45;
    iNewImgSize = iNewBtnSize / 4 * 3;
    $(".KToolbarImg").each(function () {
        $(this).css("width", iNewImgSize + "px");
        $(this).css("height", iNewImgSize + "px");
        $(this).css("border-radius", iNewImgSize / 5 + "px");
    });
}




//////////////////////////////////////////////////////////////////////////////////////
// 在加载完成后
//$(document).ready(function () {
$(function () {

    //$(".footer").hide();

    // 启用tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    // 启用popover
    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    // websocket连接定时器
    let timeID = self.setInterval(function () {
        if (null == kWebSocket) {// 自动连接重连
            KSendMsg(JSON.stringify({ "msgType": "hello" }));
        }
    }, 1000);



    // 在容器大小改变时，图表跟随改变大小
    window.addEventListener("resize", KWindowResize);
    KWindowResize();
    // 下拉隐藏功能的图片切换
    $('.collapseKeyBoardBody').on('hide.bs.collapse', function () {
        //$('#cardHeaderKeyBoardButtons .KCollapseImg').attr("src", "images/collapse1.png");
        $(this).prev("div").find('img.KCollapseImg').attr("src", "images/collapse1.png");
    })
    $('.collapseKeyBoardBody').on('show.bs.collapse', function () {
        //$('#cardHeaderKeyBoardButtons .KCollapseImg').attr("src", "images/collapse2.png");
        $(this).prev("div").find('img.KCollapseImg').attr("src", "images/collapse2.png");
    })

    // 所有弹出窗口已经显示
    $('.KModal').on('shown.bs.modal', function (e) {
        //console.log('窗口已经显示');
        //console.log(e);
        if (gModalDialogID != null) {
            $('#' + gModalDialogID).modal('hide');
        }
        gModalDialogID = e.target.id;
    })
    // 所有弹出窗口已经隐藏
    $('.KModal').on('hidden.bs.modal', function (e) {
        //console.log('窗口已经隐藏');
        //console.log(e);
        gModalDialogID = null;
        KSendMsg(JSON.stringify({ "msgType": "modal-btnClose" }));
    })
    // 开始测试设置弹窗显示事件
    $('#modalStartTestSet').on('shown.bs.modal', function (e) {
        //console.log('#modalStartTestSet show');
        // 显示当前模式对应的tab
        if (gTestMode == EnumTestMode.EnumTestMode_NormalTest) {
            $('#nav-tab-StartSetting a[href="#nav-NormalTest"]').tab('show');
        }
        else if (gTestMode == EnumTestMode.EnumTestMode_TriggerTest) {
            $('#nav-tab-StartSetting a[href="#nav-TriggerTest"]').tab('show')
        }
        else {
            $('#nav-tab-StartSetting a[href="#nav-NormalTest"]').tab('show');
        }
        $("#nav-TriggerTest .CurrentUnit").text('(' + gUnit[SampleTupleNameEN(SampleTupleIndex.SampleTuple_Current)] + ')');

        modalStartTestSetTabChange();
    })
    //$('#modalStartTestSet').on('hidden.bs.modal', function (e) {// 窗口已经隐藏
    //    console.log("开始测试参数配置窗口已隐藏");
    //    //KSendMsg(JSON.stringify({ "msgType": "modalStartTestSet-btnClose" }));
    //})
    // 开始测试设置的Tab页的改变
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab

        if (e.target.href.indexOf("nav-NormalTest") != -1
            || e.target.href.indexOf("nav-TriggerTest") != -1) {
            modalStartTestSetTabChange();
        }
        //console.log(e.target.href);
        //console.log(e.relatedTarget.href);
    })

    $(".footerText").text('窗口宽度：' + $(window).width() + 'px');





    $('#kkkkkk').on("click", function () {
        console.log($("#kkkkkk"));
        KSendMsg(JSON.stringify({ "msgType": "click" }));
    });
    $('#kkkkkk').on("mousedown", function () {
        KSendMsg(JSON.stringify({ "msgType": "mousedown" }));
    });
    $('#kkkkkk').on("mouseup", function () {
        KSendMsg(JSON.stringify({ "msgType": "mouseup" }));
    });
    $('#kkkkkk').on("keydown", function () {
        KSendMsg(JSON.stringify({ "msgType": "keydown" }));
    });
    $('#kkkkkk').on("keyup", function () {
        KSendMsg(JSON.stringify({ "msgType": "keyup" }));
    });
     
    $('#kkkkkk').on("touchstart", function () {
        KSendMsg(JSON.stringify({ "msgType": "touchstart" }));
    });
    $('#kkkkkk').on("touchmove", function () {
        KSendMsg(JSON.stringify({ "msgType": "touchmove" }));
    });
    $('#kkkkkk').on("touchend", function () {
        KSendMsg(JSON.stringify({ "msgType": "touchend" }));
    });
    


});

function ScreenShotZoomOut() {
    let imgOrgWidth = parseInt($('#screenShotImgContainer').attr('imgOrgWidth'));
    let imgOrgHeight = parseInt($('#screenShotImgContainer').attr('imgOrgHeight'));
    let imgWidth = parseInt($('#screenShotImgContainer').attr('imgWidth'));
    let imgHeight = parseInt($('#screenShotImgContainer').attr('imgHeight'));
    //$('#screenShotImgContainer img').css('transform', 'scale(0.5)');

    imgWidth = Math.ceil(imgWidth * 0.8);
    imgHeight = Math.ceil(imgWidth / imgOrgWidth * imgOrgHeight);

    $('#screenShotImg').css('width', Math.ceil(imgWidth) + 'px');
    $('#screenShotImg').css('height', Math.ceil(imgHeight) + 'px');

    $('#screenShotImgContainer').attr('imgWidth', imgWidth);
    $('#screenShotImgContainer').attr('imgHeight', imgHeight);


}

function ScreenShotZoomIn() {
    let imgOrgWidth = parseInt($('#screenShotImgContainer').attr('imgOrgWidth'));
    let imgOrgHeight = parseInt($('#screenShotImgContainer').attr('imgOrgHeight'));
    let imgWidth = parseInt($('#screenShotImgContainer').attr('imgWidth'));
    let imgHeight = parseInt($('#screenShotImgContainer').attr('imgHeight'));
    //$('#screenShotImgContainer img').css('transform', 'scale(0.5)');

    imgWidth = Math.ceil(imgWidth * 1.2);
    imgHeight = Math.ceil(imgWidth / imgOrgWidth * imgOrgHeight);

    $('#screenShotImg').css('width', Math.ceil(imgWidth) + 'px');
    $('#screenShotImg').css('height', Math.ceil(imgHeight) + 'px');

    $('#screenShotImgContainer').attr('imgWidth', imgWidth);
    $('#screenShotImgContainer').attr('imgHeight', imgHeight);
}

function ScreenShotAdaption(elem) {
    let imgOrgWidth = parseInt($('#screenShotImgContainer').attr('imgOrgWidth'));
    let imgOrgHeight = parseInt($('#screenShotImgContainer').attr('imgOrgHeight'));
    let imgWidth = parseInt($('#screenShotImgContainer').attr('imgWidth'));
    let imgHeight = parseInt($('#screenShotImgContainer').attr('imgHeight'));
    //$('#screenShotImgContainer img').css('transform', 'scale(0.5)');

    imgWidth = $('#collapseScreenShot .card-body').width() - 25;
    imgHeight = Math.ceil(imgWidth / imgOrgWidth * imgOrgHeight);

    /*
    let maxHeight = $('#screenShotImgContainer').css('max-height');
    maxHeight = parseInt(maxHeight);
    if (maxHeight < imgHeight)
    {
        maxHeight = imgHeight + 25;
        if (maxHeight < 500)
        {
            maxHeight = 500;
        }
        $('#screenShotImgContainer').css('max-height', maxHeight + 'px');
    }
    */

    $('#screenShotImg').css('width', Math.ceil(imgWidth) + 'px');
    $('#screenShotImg').css('height', Math.ceil(imgHeight) + 'px');

    $('#screenShotImgContainer').attr('imgWidth', imgWidth);
    $('#screenShotImgContainer').attr('imgHeight', imgHeight);
}

function setGesture(el) {
    var obj = {}; //定义一个对象
    var istouch = false;
    var start = [];
    el.addEventListener("touchstart", function (e) {
        if (e.touches.length >= 2) {  //判断是否有两个点在屏幕上
            istouch = true;
            start = e.touches;  //得到第一组两个点
            obj.gesturestart && obj.gesturestart.call(el); //执行gesturestart方法
        };
    }, false);
    document.addEventListener("touchmove", function (e) {
        e.preventDefault();
        if (e.touches.length >= 2 && istouch) {
            var now = e.touches;  //得到第二组两个点
            var scale = getDistance(now[0], now[1]) / getDistance(start[0], start[1]); //得到缩放比例，getDistance是勾股定理的一个方法
            var rotation = getAngle(now[0], now[1]) - getAngle(start[0], start[1]);  //得到旋转角度，getAngle是得到夹角的一个方法
            e.scale = scale.toFixed(2);
            e.rotation = rotation.toFixed(2);
            obj.gesturemove && obj.gesturemove.call(el, e);  //执行gesturemove方法
        };
    }, false);
    document.addEventListener("touchend", function (e) {
        if (istouch) {
            istouch = false;
            obj.gestureend && obj.gestureend.call(el);  //执行gestureend方法
        };
    }, false);
    return obj;
};
function getDistance(p1, p2) {
    var x = p2.pageX - p1.pageX,
        y = p2.pageY - p1.pageY;
    return Math.sqrt((x * x) + (y * y));
};
function getAngle(p1, p2) {
    var x = p1.pageX - p2.pageX,
        y = p1.pageY - p2.pageY;
    return Math.atan2(y, x) * 180 / Math.PI;
};

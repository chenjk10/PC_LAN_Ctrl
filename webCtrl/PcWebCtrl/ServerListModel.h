﻿#ifndef SERVERLISTMODEL_H
#define SERVERLISTMODEL_H

#include <QAbstractListModel>
#include "UdpBroadcast.h"

class ServerListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static ServerListModel * Singleton();
public:
    enum BluetoothDeviceRole {
        ServerNameRole = Qt::DisplayRole,
        ServerAddrRole = Qt::UserRole,
    };
    Q_ENUM(BluetoothDeviceRole)

    ServerListModel(QObject *parent = nullptr);

    virtual int rowCount(const QModelIndex & = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

public slots:
    Q_INVOKABLE QVariantMap get(int row) const;
    // 增加
    Q_INVOKABLE void addServer(const QString& serverName, const QString& serverAddr);
    // 删除行
    Q_INVOKABLE void remove(int row);
    // 清空
    Q_INVOKABLE void clear();
public slots:
    void scanServer();
private:
    class ServerInfo
    {
    public:
        QString serverName;
        QString serverAddr;
    public:
        ServerInfo()
        {}
        ServerInfo(const QString& serverName, const QString& serverAddr)
        {
            this->serverName = serverName;
            this->serverAddr = serverAddr;
        }
    };

    QList<ServerInfo> mServerInfoList;

    UdpBroadcast udpBroadcastClient;
};

#endif // SERVERLISTMODEL_H

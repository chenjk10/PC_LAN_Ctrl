﻿import QtQuick 2.13
import QtQuick.Controls 2.13
import QtWebView 1.14

// 导入自定义包
import com.mycompany.customType 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("控制器")

    // 字体加载
    FontLoader {
        id: customFont;
        source: "/font/SourceHanSansCN-Normal.otf"
    }
    // 设置字体,默认字体中文显示有问题
    font{family: customFont.name; pixelSize: 14}


    header: ToolBar {
        height: 30
        //contentHeight: 15//toolButton.implicitHeight
        Row{
            anchors.fill: parent
            ToolButton {
                //id: toolButton
                height: parent.height
                width: parent.height
                text: "\u2630"
                icon.source: {
                    if(0 === swipeView.currentIndex)
                    {
                        return "/image/refresh.png"
                    }
                    else if(1 === swipeView.currentIndex)
                    {
                        return "/image/return.png"
                    }
                    return "/image/menuBtn.png"
                }
                display: AbstractButton.IconOnly
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    if(0 === swipeView.currentIndex)
                    {
                        listViewServer.model.scanServer();
                    }
                    else if(1 === swipeView.currentIndex)
                    {
                        swipeView.currentIndex = 0;
                    }
                }
            }
            ToolButton {
                //id: toolButton
                height: parent.height
                width: parent.height
                visible: (1 === swipeView.currentIndex?true:false)
                text: "\u2630"
                icon.source: "/image/refresh.png"
                display: AbstractButton.IconOnly
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    webView.reload();
                }
            }
        }

        Label {
            id: labelWebTitle
            text: qsTr("设备列表")
            anchors.centerIn: parent
        }
    }

    // 服务设备列表delegate-------------------------------------------------------
    Component {
        id: delegateServerList
        //
        Item {
            id: delegateServerItem
            property int itemShowHeight: columnDevice.height + 2 * columnDevice.spacing
            x: 0
            width: listViewServer.width - x
            height: {
                return itemShowHeight;
            }
            opacity: 1
            smooth: true
            visible: true

            // 背景
            Rectangle {
                x: 2; y: 2;
                width: parent.width - x*2
                height: parent.height - y*2
                color: "ivory"
                border.color: "orange"
                radius: 5
            }
            // 鼠标响应
            MouseArea {
                anchors.fill: parent
                //propagateComposedEvents: true
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    //console.log("onClicked ServerItem:" + index)
                    mouse.accepted = false;
                    listViewServer.currentIndex = index
                    // 强制获取焦点，否则无法使用键盘上下键切换行
                    listViewServer.forceActiveFocus();
                }
            }
            Item {
                //spacing: 10
                anchors.fill: parent
                Column{
                    id: columnDevice
                    spacing: 8
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.leftMargin: 10
                    //anchors.topMargin: 5
                    //anchors.bottomMargin: 5
                    Text {
                        //id: textDeviceName
                        width: parent.width
                        wrapMode: TextArea.WrapAnywhere
                        //topPadding: 2
                        lineHeight: 0.7
                        text: {
                            return serverName.length > 0 ? serverName : "未知"
                        }
                        font.family: customFont.name
                        font.bold: true
                        font.pixelSize: 15
                    }
                    Text {
                        //id: textDeviceAddr
                        width: parent.width
                        wrapMode: TextArea.WrapAnywhere
                        //topPadding: 2
                        lineHeight: 0.7
                        text: {
                            return serverAddr;
                        }
                        anchors.topMargin: 2
                        font.family: customFont.name
                        //font.bold: true
                        font.pixelSize: 11
                        color: "gray"
                    }
                }
                RoundButton {
                    id: btnServerConnect
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    anchors.verticalCenter: parent.verticalCenter
                    focus: true
                    visible: true
                    text: {
                        return qsTr("打开");
                    }
                    background: Rectangle {
                        //implicitWidth: delegateServerItem.height
                        //implicitHeight: delegateServerItem.height
                        radius: delegateServerItem.height / 2
                        opacity: enabled ? 1 : 0.3
                        color: btnServerConnect.down ? "#d0d0d0" : "#e0e0e0"
                        border.color: "lightgreen"
                        scale: btnServerConnect.down ? 1.1 : 1.0
                    }
                    onClicked: {
                        listViewServer.currentIndex = index
                        console.log("click on btnServerConnect:" + text + "  " + index + "  " + serverName + "   " + serverAddr);
                        webView.url = serverAddr;
                        swipeView.currentIndex = 1;
                    }
                }
            }
            // 状态值
            states: [
                /*, State {// 当前选择行为当前
                    name: "Current"
                    when: delegateServerItem.ListView.isCurrentItem
                    PropertyChanges {
                        target: delegateServerItem;
                        x: 20
                    }
                }*/
            ]
            // 属性变化动画效果
            transitions: [
                Transition {
                    PropertyAnimation { property: "height"; duration: 250; }
                }
                /*, Transition {// 没效果，原因未知，使用Behavior有效果
                    //PropertyAnimation { property: "opacity"; duration: 300; }
                }*/
                /*
                , Transition {
                    NumberAnimation { properties: "x,y,contentY,height,width"; duration: 200 }
                }*/
            ]
            Behavior on opacity {
                PropertyAnimation { duration: 250 }
            }

            // 增删动画效果Animate adding and removing of items:
            ListView.onAdd: SequentialAnimation {
                PropertyAction { target: delegateServerItem; property: "height"; value: 0 }
                NumberAnimation { target: delegateServerItem; property: "height"; to: delegateServerItem.height; duration: 250; easing.type: Easing.InOutQuad }
            }
            ListView.onRemove: SequentialAnimation {
                PropertyAction { target: delegateServerItem; property: "ListView.delayRemove"; value: true }
                NumberAnimation { target: delegateServerItem; property: "height"; to: 0; duration: 250; easing.type: Easing.InOutQuad }

                // Make sure delayRemove is set back to false so that the item can be destroyed
                PropertyAction { target: delegateServerItem; property: "ListView.delayRemove"; value: false }
            }
        }
    }

    // 项高亮组件
    Component {
        id: highlightDevice
        Rectangle {
            // parent在列表不显示时会变为null
            // parent.parent是ListView
            x: parent&&parent.parent.currentItem ? parent.parent.currentItem.x : 0
            y: parent&&parent.parent.currentItem ? parent.parent.currentItem.y : 0
            width: parent&&parent.parent.currentItem ? parent.parent.currentItem.width : 0
            height: parent&&parent.parent.currentItem ? parent.parent.currentItem.height : 0
            color: "lightsteelblue"
            border.color: "lightgreen"
            border.width: 4
            radius: 5
            Behavior on y {
                SpringAnimation { spring: 3; damping: 0.3 }
            }
        }
    }

    // 主区域
    SwipeView {
        id: swipeView
        anchors.fill: parent
        interactive: false
        //currentIndex: tabBar.currentIndex

        ListView {
            id: listViewServer
            //anchors.fill: parent
            focus: true
            cacheBuffer: 1000000000 // 这个值不能太小，否则itemAtIndex会获取值为空，遍历的时候就会失败
            headerPositioning: ListView.OverlayHeader
            delegate: delegateServerList
            //model: bluetoothDeviceModel
            //model: BluetoothDeviceModelSingleton
            model: ServerListModel {
                id: serverListModel
                objectName: "serverListModel"
            }
            highlight: highlightDevice
            highlightFollowsCurrentItem: false
        }
        WebView {
            id: webView
            url: initialUrl
            onLoadingChanged: {
                if (loadRequest.errorString)
                {
                    console.error(loadRequest.errorString);
                }
            }
            onTitleChanged: {
                if(1 === swipeView.currentIndex)
                {
                    labelWebTitle.text = title
                }
            }
        }
        //
        onCurrentIndexChanged: {
            if(0 === swipeView.currentIndex)
            {
                labelWebTitle.text = webView.title;
            }
            else if(1 === swipeView.currentIndex)
            {
                labelWebTitle.text = qsTr("设备列表")
            }
        }
    }

}
